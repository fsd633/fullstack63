package day03;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

public class UpdateDemo1 {
    public static void main(String[] args) {

        Connection con = DbConnection.getConnection();
        PreparedStatement pst = null;

        Scanner scan = new Scanner(System.in);

        System.out.println("Enter Employee Name and New empid: ");
        String empName = scan.next();
        String empId = scan.next();
//        double newSalary = scan.nextDouble();
        System.out.println();

        String updateQuery = "UPDATE employee SET EmpId = ? WHERE empName = ?";

        try {
            pst = con.prepareStatement(updateQuery);

            pst.setString(1, empId);
            pst.setString(2, empName);

            int result = pst.executeUpdate();

            if (result > 0) {
                System.out.println("Salary updated successfully for Employee ID: " + empName);
            } else {
                System.out.println("Failed to update salary for Employee ID: " + empName);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    pst.close();
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
