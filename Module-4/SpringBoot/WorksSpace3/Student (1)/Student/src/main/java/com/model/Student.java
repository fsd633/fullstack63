package com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Student {
	
	
	@Id@GeneratedValue
	private int stuID;
	
	@Column(name= "StudentName")
	private String studentName;
	private int age;
	private String course;
	private long mobile;
	private String emailId;
	private int password;
	
	/*
	 @Entity :Makes the normal class as an entity class
	 @Id : make the variable name column as a primary key column 
	@column : provides the name for the colummn rather than the provided varibale name
	@generatedvalue: makes the primary key column value
	 */
	
	
	
	public int getStuID() {
		return stuID;
	}

	public void setStuID(int stuID) {
		this.stuID = stuID;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}

	public long getMobile() {
		return mobile;
	}

	public void setMobile(long mobile) {
		this.mobile = mobile;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public int getPassword() {
		return password;
	}

	public void setPassword(int password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Student [stuID=" + stuID + ", studentName=" + studentName + ", age=" + age + ", course=" + course
				+ ", mobile=" + mobile + ", emailId=" + emailId + ", password=" + password + "]";
	}

	public Student(int stuID, String studentName, int age, String course, long mobile, String emailId, int password) {
		super();
		this.stuID = stuID;
		this.studentName = studentName;
		this.age = age;
		this.course = course;
		this.mobile = mobile;
		this.emailId = emailId;
		this.password = password;
	}

	public Student(){
		
		}


	

}