package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Employee;

@Service
public class EmployeeDao {

	@Autowired
	EmployeeRepository empRepo;

	public List<Employee> getAllEmployees() {
		return empRepo.findAll();
	}
	
}