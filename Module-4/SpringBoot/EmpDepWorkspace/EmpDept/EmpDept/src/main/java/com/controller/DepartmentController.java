package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dao.DepartmentDao;
import com.model.Department;

@RestController
public class DepartmentController {

	@Autowired
	DepartmentDao deptDao;
	
	@GetMapping("getAllDepartments")
	public List<Department> getAllDepartments() {
		return deptDao.getAllDepartments();
	}
	
}