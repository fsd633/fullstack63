package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Department;

@Service
public class DepartmentDao {

	@Autowired
	DepartmentRepository deptRepo;

	public List<Department> getAllDepartments() {
		return deptRepo.findAll();
	}
	
}