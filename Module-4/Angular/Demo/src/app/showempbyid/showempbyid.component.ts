import { Component } from '@angular/core';

@Component({
  selector: 'app-showempbyid',
  templateUrl: './showempbyid.component.html',
  styleUrl: './showempbyid.component.css'
})
export class ShowempbyidComponent {

  msg: any;
  emp: any;
  employees: any;
  emailId:any;
  constructor() {
    this.msg = "";
    this.emailId = localStorage.getItem('emailId');
    this.employees = [
      {empId:101, empName:'Harsha', salary:1212.12, gender:'Male',   country:'India', doj:'25-12-2018', mobile:'9876543210', emailId:'harsha@gmail.com', password:'123'},
      {empId:102, empName:'Pasha',  salary:1212.12, gender:'Male',   country:'India', doj:'26-11-2017', mobile:'9876543210', emailId:'pasha@gmail.com',  password:'123'},
      {empId:103, empName:'Indira', salary:1212.12, gender:'Female', country:'India', doj:'27-10-2016', mobile:'9876543210', emailId:'indira@gmail.com', password:'123'},
      {empId:104, empName:'Venkat', salary:1212.12, gender:'Male',   country:'India', doj:'28-11-2015', mobile:'9876543210', emailId:'venkat@gmail.com', password:'123'},
      {empId:105, empName:'Gopi',   salary:1212.12, gender:'Male',   country:'India', doj:'29-12-2014', mobile:'9876543210', emailId:'gopi@gmail.com',   password:'123'},
      {empId:105, empName:'Tharun',   salary:1212.12, gender:'Male',   country:'India', doj:'12-29-2014', mobile:'9876543210', emailId:'@gmail.com',   password:'123'}

    ];
  }

  getEmployee(empForm: any) {

    this.emp = null;

    this.employees.forEach((element: any) => {
      if (element.empId == empForm.empId) {
        this.emp = element;
        this.msg = "";
      } else {
        this.msg = "Employee Record Not Found!!!";
      }
    });

  }
}