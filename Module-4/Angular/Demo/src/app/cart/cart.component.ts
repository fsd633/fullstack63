import { Component } from '@angular/core';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrl: './cart.component.css'
})
export class CartComponent {

  emailId: any;
  localData: any;
  cart: any;
  total: number;

  constructor() {
    this.total = 0;
    console.log(this.cart);

    this.emailId = localStorage.getItem('emailId');

    this.localData = localStorage.getItem('cartItems');
    this.cart = JSON.parse(this.localData);

    console.log(this.cart);

    if (this.cart != null) {
      this.cart.forEach((element: any) => {
        this.total += element.price;
      });
    }
  }

  purchase() {
    this.cart='';
    this.localData = null;
    this.total = 0;
    localStorage.removeItem('cartItems');
  }
}


