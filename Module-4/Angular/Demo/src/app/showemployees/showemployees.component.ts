import { Component } from '@angular/core';

@Component({
  selector: 'app-showemployees',
  templateUrl: './showemployees.component.html',
  styleUrl: './showemployees.component.css'
})

export class ShowemployeesComponent {

  employees: any;
  emailId:any;
  constructor() {
    this.emailId = localStorage.getItem('emailId');
    this.employees = [
      {empId:101, empName:'Harsha', salary:1212.12, gender:'Male',   country:'India', doj:'12-25-2018', mobile:'9876543210', emailId:'harsha@gmail.com', password:'123'},
      {empId:102, empName:'Pasha',  salary:1212.12, gender:'Male',   country:'India', doj:'11-26-2017', mobile:'9876543210', emailId:'pasha@gmail.com',  password:'123'},
      {empId:103, empName:'Indira', salary:1212.12, gender:'Female', country:'India', doj:'10-27-2016', mobile:'9876543210', emailId:'indira@gmail.com', password:'123'},
      {empId:104, empName:'Venkat', salary:1212.12, gender:'Male',   country:'India', doj:'11-28-2015', mobile:'9876543210', emailId:'venkat@gmail.com', password:'123'},
      {empId:105, empName:'Gopi',   salary:1212.12, gender:'Male',   country:'India', doj:'12-29-2014', mobile:'9876543210', emailId:'gopi@gmail.com',   password:'123'},
      {empId:105, empName:'Tharun',   salary:1212.12, gender:'Male',   country:'India', doj:'12-29-2014', mobile:'9876543210', emailId:'tharun@gmail.com',   password:'123'}

    ];
  }

}