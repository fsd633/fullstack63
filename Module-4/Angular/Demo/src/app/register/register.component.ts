import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent implements OnInit {
  
  countries: any;

  //Dependency Injection for EmpService
  constructor(private service: EmpService) {
  }

  ngOnInit() {
    this.service.getAllCountries().subscribe((data: any) => {
      console.log(data);
      this.countries = data;
    });
  }

  registerSubmit(regForm: any) {
    console.log(regForm);
  }

}


