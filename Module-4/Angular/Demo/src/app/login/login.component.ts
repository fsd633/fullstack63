import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent implements OnInit {
  
  employees: any;
  emp:any;
  
  
  
  //Implementing Dependency Injection for Router Class from RouterModule & EmpService
  constructor(private router: Router, private service: EmpService) {

    this.employees = [
      {empId:101, empName:'Harsha', salary:1212.12, gender:'Male',   country:'India', doj:'25-12-2018', mobile:'9876543210', emailId:'harsha@gmail.com', password:'123'},
      {empId:102, empName:'Pasha',  salary:1212.12, gender:'Male',   country:'India', doj:'26-11-2017', mobile:'9876543210', emailId:'pasha@gmail.com',  password:'123'},
      {empId:103, empName:'Indira', salary:1212.12, gender:'Female', country:'India', doj:'27-10-2016', mobile:'9876543210', emailId:'indira@gmail.com', password:'123'},
      {empId:104, empName:'Venkat', salary:1212.12, gender:'Male',   country:'India', doj:'28-11-2015', mobile:'9876543210', emailId:'venkat@gmail.com', password:'123'},
      {empId:105, empName:'Gopi',   salary:1212.12, gender:'Male',   country:'India', doj:'29-12-2014', mobile:'9876543210', emailId:'gopi@gmail.com',   password:'123'}
    ];
  }

  ngOnInit() {

  
  }


  
  loginSubmit(loginForm: any) {
    console.log(loginForm);
    
    if (loginForm.emailId == 'HR' && loginForm.password == 'HR') {     
      //Making isUserLoggedIn value to true under EmpService
      this.service.setIsUserLoggedIn();
      this.router.navigate(['showemps']);
    } else {

      this.emp = null;

      this.employees.forEach((element: any) => {
        if (element.emailId == loginForm.emailId && element.password == loginForm.password) {
          this.emp = element;
        }
      });

      if (this.emp != null) {
        //Making isUserLoggedIn value to true under EmpService
        this.service.setIsUserLoggedIn();
        this.router.navigate(['products']);
      } else {
        alert('Invalid Credentials');
      }
    }
  }


}


