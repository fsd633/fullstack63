import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test2',
  templateUrl: './test2.component.html',
  styleUrl: './test2.component.css'
})
export class Test2Component implements OnInit{
 
 person:any;
  constructor(){
   this.person = {
    id: 101,
    name:"Dhoni",
    age:22,
    address:{streetNo:12 , city:"Hyd" ,state:"telangana"},
    Hobbies:['Foodie','sleeping','Cricket','singing','ScrollingReels']
   };

 }
 
  ngOnInit(){
    
  }

  btnSubmit(){
    console.log(this.person);
  }
  
}