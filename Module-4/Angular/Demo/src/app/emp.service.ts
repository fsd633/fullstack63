import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmpService {

  isUserLoggedIn: boolean;
  
  constructor(private http: HttpClient) {
    this.isUserLoggedIn = false;
  }

  //Logged In Successfully
  setIsUserLoggedIn() {
    this.isUserLoggedIn = true;
  }
  getIsUserLoggedIn(): boolean {
    return this.isUserLoggedIn;
  }
  setIsUserLoggedOut() {
    this.isUserLoggedIn = false;
  }

  getAllCountries(): any {
    return this.http.get('https://restcountries.com/v3.1/all');
  }
}




